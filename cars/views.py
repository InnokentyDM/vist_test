from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView
from django_filters.views import FilterView

from cars.models import Cars, CarModels


class CarsListView(FilterView):
    template_name = 'cars_list.html'
    model = Cars
    filterset_fields = ['car_model']