from django.db import models

# Create your models here.

class CarModels(models.Model):
    name = models.CharField(max_length=50, verbose_name='Модель', unique=True)
    max_load_capacity = models.IntegerField(verbose_name='Грузоподъемность')


    class Meta:
        verbose_name = 'Модель'
        verbose_name_plural = 'Модели'

    def __str__(self):
        return '{}'.format(self.name)


class Cars(models.Model):
    car_model = models.ForeignKey(CarModels, on_delete=models.PROTECT, verbose_name='Модель')
    tail_number = models.CharField(max_length=4, unique=True, verbose_name='Бортовой номер')
    current_load_capacity = models.IntegerField(verbose_name='Текущая загрузка')

    @property
    def overload(self):
        if self.current_load_capacity > self.car_model.max_load_capacity:
            return "{0:.2f}".format(self.current_load_capacity/self.car_model.max_load_capacity*100-100)
        return 0

    class Meta:
        verbose_name = 'Самосвал'
        verbose_name_plural = 'Самосвалы'

    def __str__(self):
        return '{}:{}'.format(self.car_model.name, self.tail_number)