from django.forms import ModelForm

from cars.models import CarModels


class CarsForm(ModelForm):
    class Meta:
        model = CarModels
        fields = ['name', ]
