from django.contrib import admin

# Register your models here.
from cars.models import CarModels, Cars


@admin.register(CarModels)
class CarsModelsAdmin(admin.ModelAdmin):
    list_display = ('name', 'max_load_capacity')

@admin.register(Cars)
class CarsAdmin(admin.ModelAdmin):
    list_display = ('car_model', 'tail_number', 'current_load_capacity')