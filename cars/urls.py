from django.urls import path

from cars.views import CarsListView

urlpatterns = [
    path('list/', CarsListView.as_view(), name='cars-list'),
]